from django.shortcuts import get_object_or_404, render

# Create your views here.

from time import strftime
import datetime
from django.http import HttpResponse, HttpResponseRedirect
from .models import Blog, Comment
from django.urls import reverse
import random as rand;

generated_content = '''
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sed neque aliquam, viverra est eget, rhoncus urna. Cras vel lacinia nulla. Curabitur quis ornare sem. Maecenas posuere rhoncus elit ac ultricies. Maecenas sodales, libero ut interdum ultrices, turpis libero maximus justo, et ultricies ante arcu at nibh. Nullam vehicula, eros eget dictum posuere, nunc nisl pharetra est, non dignissim libero nunc a nibh. Nullam cursus cursus dolor eget bibendum. Suspendisse ac sollicitudin dolor, sit amet ullamcorper neque. Phasellus fermentum nisl a nibh consequat, quis porttitor justo efficitur.

Curabitur accumsan tortor quis luctus fermentum. Aenean neque ipsum, consectetur vel dapibus sed, molestie quis risus. Ut ac ultricies risus. Ut imperdiet, dui eu feugiat tincidunt, ex tortor placerat diam, eget pellentesque mi tortor in sem. Maecenas tristique venenatis eros, vitae rutrum nisl molestie nec. Nulla laoreet ornare justo sit amet maximus. Donec efficitur turpis vitae ex cursus tincidunt. Sed ut neque ut velit ultricies rutrum nec sit amet erat. Aliquam fermentum in metus vel mollis. Mauris ligula massa, lacinia id vulputate sit amet, congue at nulla. Ut efficitur porttitor sollicitudin.

Etiam in feugiat dolor, nec feugiat augue. Nulla sit amet augue urna. Suspendisse porttitor dolor a ligula mollis bibendum venenatis eget nisi. Praesent blandit et ex sed convallis. Maecenas malesuada tempor tellus nec laoreet. Vestibulum fermentum sodales mauris in dictum. Cras fermentum gravida sapien nec finibus. Morbi eget sollicitudin nunc. Ut dapibus cursus interdum. Vivamus aliquet vestibulum nibh, id tempor sem scelerisque ac. Phasellus et pulvinar quam. Vivamus nec convallis orci. Morbi lectus augue, hendrerit laoreet erat ut, fringilla sollicitudin ante. In tempus tincidunt volutpat. Vivamus lectus leo, sodales nec lacinia eu, vulputate a arcu.

Quisque porttitor suscipit finibus. Aenean at condimentum augue. Nulla varius metus a tortor euismod laoreet. Etiam quis nisi ante. Praesent imperdiet, ipsum id euismod efficitur, purus ante luctus leo, ut vestibulum sem justo mattis diam. Donec aliquet porttitor lectus, vel fermentum augue. Morbi ultrices metus lectus, et lobortis mi auctor in. Suspendisse scelerisque dui quis elit sagittis condimentum. Nam tincidunt odio in orci posuere auctor ac cursus odio. Morbi dapibus, quam sed placerat congue, justo ex cursus mi, vel euismod sapien metus vel lectus. Pellentesque tristique nulla ut ullamcorper suscipit. Nulla tellus mi, imperdiet a orci ac, finibus varius orci. Donec sit amet interdum nulla. Sed luctus aliquam arcu sed maximus. Nunc ut vestibulum enim. Nullam tempor magna at suscipit sollicitudin.

Vivamus luctus mauris at risus pellentesque ornare. Donec aliquet consequat lacus, eget vestibulum dui cursus et. Mauris eleifend, velit nec cursus ultrices, eros erat aliquam tortor, eu vulputate sapien metus viverra orci. Pellentesque pharetra felis est, eu tempor arcu convallis ac. Donec ac sodales purus. Donec euismod ex ante, eu lobortis libero sodales sed. In hac habitasse platea dictumst.

'''

generated_comment = '''
This is a generated comment. Yay!
'''


def nuke(request):
    for b in Blog.objects.all():
        b.delete()
    for c in Comment.objects.all():
        c.delete()
    return HttpResponseRedirect(reverse('blog:index'))


def index(request):
    now = strftime('%c')
    return render(request, 'blog/index.html', {
        'now': now
    })


def init(request):
	nuke(request)
	for i in range(10):
            blog = Blog(title="Title" + str(i), content=generated_content,
                        author="Someone important", posted_date=datetime.datetime.now())
            blog.save()
            for j in range(i):
                comment = Comment(title="title" + str(rand.randint(0,100)), author = "jd03", blog = blog, content = generated_comment, posted_date = datetime.datetime.now(), email = "example@example.com")
                comment.save()
	return HttpResponseRedirect(reverse('blog:blogHome'))


def blogHome(request):
    now = strftime('%c')
    blogs = Blog.objects.order_by('-posted_date')[:3]
    return render(request, 'blog/blog_home.html', {
        'now': now,
        'blogs': blogs,
    })

def blogArchive(request):
    blogs = Blog.objects.order_by('-posted_date')
    return render(request, 'blog/blog_archive.html', {
        'blogs': blogs,
    })


def aboutMe(request):
    now = strftime('%c')
    return render(request, 'blog/about_me.html', {
        'now': now
    })


def techTips(request):
    now = strftime('%c')
    return render(request, 'blog/tech_tips.html', {
        'now': now
    })


def viewBlog(request, blog_id):
    blog = get_object_or_404(Blog, pk=blog_id)
    return render(request, 'blog/blog_view.html', {
        'blog': blog,
        'comments':Comment.objects.filter(blog=blog).order_by('-posted_date'),
    })
    #adsf

def comment(request, blog_id):
    postData = request.POST
    comment = Comment(title=postData['title'], author=postData['author'], email=postData['email'], content=postData['content'], posted_date = datetime.datetime.now(), blog=Blog.objects.get(pk=blog_id))
    comment.save()
    return HttpResponseRedirect(reverse('blog:blog', args=[blog_id]))