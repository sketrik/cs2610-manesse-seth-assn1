from django.db import models

# Create your models here.
class Blog(models.Model):
	title=models.CharField(max_length = 200)
	author=models.CharField(max_length = 200)
	content=models.TextField(max_length = 100000)
	posted_date = models.DateTimeField('date posted')
	def numComments(self):
		return Comment.objects.filter(blog = self).count()
	comments = property(numComments)
	
class Comment(models.Model):
	blog = models.ForeignKey(Blog, on_delete=models.CASCADE)
	title=models.CharField(max_length = 200)
	author=models.CharField(max_length = 200)
	content=models.TextField(max_length = 10000)
	posted_date = models.DateTimeField('date posted')
	email = models.EmailField(max_length=254, default="example@example.com")