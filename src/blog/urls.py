from django.urls import path

from . import views

app_name='blog'

urlpatterns = [
    path('', views.index, name='index'),
    path('init', views.init, name='init'),
    path('blogHome', views.blogHome, name='blogHome'),
    path('blogArchive', views.blogArchive, name='blogArchive'),
    path('aboutMe', views.aboutMe, name='aboutMe'),
    path('techTips', views.techTips, name='techTips'),
    path('blog/<int:blog_id>/', views.viewBlog, name='blog'),
    path('<int:blog_id>/comment/', views.comment, name='comment'),
]